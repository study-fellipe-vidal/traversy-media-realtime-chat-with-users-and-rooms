const users = [];

export function userJoin(newUser) {
  users.push(newUser);
  return newUser;
}

export function getCurrentUser(userId) {
  return users.find((user) => user.id === userId);
}

export function userLeave(userId) {
  const index = users.findIndex((user) => user.id === userId);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
}

export function getRoomUsers(room) {
  return users.filter((user) => user.room === room);
}
