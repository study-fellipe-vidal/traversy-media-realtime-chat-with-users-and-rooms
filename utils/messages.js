import { format } from "date-fns";

export function formatMessage(username, message) {
  return {
    username,
    message,
    time: format(Date.now(), "h:mm a"),
  };
}
