import express from "express";
import http from "http";
import { Server } from "socket.io";

import { formatMessage } from "./utils/messages.js";
import {
  userJoin,
  getCurrentUser,
  userLeave,
  getRoomUsers,
} from "./utils/user.js";

const BOT_NAME = "ChatCord Bot";

const app = express();
const server = http.createServer(app);
const io = new Server(server, { cors: "*" });

// Set static folder
app.use(express.static("public"));

// Run when client connects
io.on("connection", (socket) => {
  socket.on("join-room", ({ username, room }) => {
    const user = userJoin({ id: socket.id, username, room });
    socket.join(user.room);

    // Welcome new user
    socket.emit("message", formatMessage(BOT_NAME, "Welcome to ChatCord!"));

    // Broadcast when a ser connects
    socket.broadcast
      .to(user.room)
      .emit(
        "message",
        formatMessage(BOT_NAME, `${user.username} has joined the chat`)
      );

    // Send users and room info
    io.to(user.room).emit("room-users", {
      room: user.room,
      users: getRoomUsers(user.room),
    });

    // Listen for chat-message
    socket.on("chat-message", (msg) => {
      const user = getCurrentUser(socket.id);
      io.to(user.room).emit("message", formatMessage(user.username, msg));
    });
  });

  // Run when client disconnects
  socket.on("disconnect", () => {
    const user = userLeave(socket.id);

    if (user) {
      io.to(user.room).emit(
        "message",
        formatMessage(BOT_NAME, `${user.username} has left the chat`)
      );

      // Send users and room info
      io.to(user.room).emit("room-users", {
        room: user.room,
        users: getRoomUsers(user.room),
      });
    }
  });
});

const PORT = 3000 || process.env.PORT;

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));
